#include "lib.h"
#include <set>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>

useconds_t game_turn_time_ms;
uint16_t port_num;
uint32_t turn_speed;
uint64_t seed;

class Board {
    int width_;
    int height_;
    set<pair<int, int>> occupied_fields_;

public:
    Board(int width, int height) : width_(width), height_(height) {}
    bool IsOnBoard(int x, int y) {
        return (0 <= x && x < width_ &&
                0 <= y && y < height_);
    }
    bool IsOccupied(int x, int y) {
        return occupied_fields_.find(make_pair(x, y)) != occupied_fields_.end();
    }
    bool SetOccupied(int x, int y) {
        if (!this->IsOnBoard(x, y)) {
            return false;
        }
        occupied_fields_.insert(make_pair(x, y));
        return true;
    }
    bool SetFree(int x, int y) {
        if (!this->IsOnBoard(x, y)) {
            return false;
        }
        occupied_fields_.erase(make_pair(x, y));
        return true;
    }
    int GetWidth() {
        return width_;
    }
    int GetHeight() {
        return height_;
    }
    void ClearBoard() {
        occupied_fields_.clear();
    }
};

class Player {
    double x_;
    double y_;
    int act_direction_;
    bool alive_;
    string name_;

public:
    Player(double x, double y, int direction, string name, Board* board) :
            x_(x), y_(y), act_direction_(direction),
            name_(name) {
        pair<int, int> coords = ConvertCoords(x_, y_);
        alive_ = !(board->IsOccupied(coords.first, coords.second));
    }
    string GetName() {
        return name_;
    }
    void MoveForward(int8_t turn_direction, Board* board) {
        if (alive_) {
            act_direction_ += turn_direction * turn_speed;
            double turn_direction_rad = act_direction_ * M_PI / 180;
            double new_x = x_ + cos(turn_direction_rad);
            double new_y = y_ + sin(turn_direction_rad);
            pair<int, int> old_coords = ConvertCoords(x_, y_);
            pair<int, int> coords = ConvertCoords(new_x, new_y);
            x_ = new_x;
            y_ = new_y;
            alive_ = (!board->IsOccupied(coords.first, coords.second) ||
                    coords == old_coords) &&
                     board->IsOnBoard(coords.first, coords.second);
            board->SetOccupied(coords.first, coords.second);
        }
    }
    bool IsAlive() {
        return this->alive_;
    }
    double GetX() {
        return this->x_;
    }
    double GetY() {
        return this->y_;
    }
};

class Client {
    sockaddr_in6 address_;
    uint64_t session_id_;
    uint32_t expected_event_;
    uint64_t last_msg_time_;
    string client_name_;
    uint8_t player_number_;
    int8_t turn_direction_;
    bool in_game_;
    bool is_ready_;

public:
    Client(sockaddr_in6 address, uint64_t session_id, string client_name) :
            address_(address), is_ready_(false), in_game_(false),
            expected_event_(0), last_msg_time_(GetTime()),
            client_name_(client_name), session_id_(session_id),
            turn_direction_(0) {}

    uint64_t GetSessionId() {
        return session_id_;
    }

    bool IsObserver() {
        return !in_game_;
    }

    bool IsReady() {
        return is_ready_;
    }

    void JoinGame() {
        expected_event_ = 0;
        in_game_ = true;
    }

    void LeaveGame() {
        in_game_ = false;
        is_ready_ = false;
    }

    int8_t GetTurnDirection() {
        return turn_direction_;
    }

    uint32_t GetNextExpectedEventNo() {
        return expected_event_;
    }

    void EventsSent(uint32_t num_events) {
        expected_event_ += num_events;
    }

    void AddDataFromMsg(CtosMsg* msg) {
        turn_direction_ = msg->turn_direction_;
        if (turn_direction_ != 0) {
            is_ready_ = true;
        }
        expected_event_ = msg->next_expected_event_no_;
        last_msg_time_ = GetTime();
    }

    bool IsActive() {
        uint64_t act_time = GetTime();
        return act_time - last_msg_time_ < 2000000ULL;
    }

    string GetName() {
        return client_name_;
    }

    bool IsWaitingForMsg(int last_event_no) {
        return expected_event_ <= last_event_no;
    }

    sockaddr_in6 GetAddress() {
        return address_;
    }
};

class Clients {
    vector<Client*> clients_;
    int first_client_to_send_;

public:
    Client* GetClientByName(string name) {
        for (uint32_t i = 0; i < clients_.size(); i++) {
            if (clients_[i]->GetName() == name) {
                return clients_[i];
            }
        }
        return NULL;
    }

    Client* GetNextClientToSend(int last_event_no) {
        DisconnectInactive();
        if (!IsAnyClientWaiting(last_event_no)) {
            return NULL;
        }
        while (true) {
            if (first_client_to_send_ >= clients_.size()) {
                first_client_to_send_ = 0;
            }
            Client* client = clients_[first_client_to_send_];
            first_client_to_send_++;
            if (client->IsWaitingForMsg(last_event_no)) {
                return client;
            }
        }
    }

    bool IsAnyClientWaiting(int last_event_no) {
        DisconnectInactive();
        for (uint32_t i = 0; i < clients_.size(); i++) {
            if (clients_[i]->IsWaitingForMsg(last_event_no)) {
                return true;
            }
        }
        return false;
    }

    Client* FindClientForSockaddrIn(
            sockaddr_in6 address, uint64_t session_id, string name) {
        DisconnectInactive();
        for (uint32_t i = 0; i < clients_.size(); i++) {
            if (clients_[i]->GetAddress() == address) {
                if (session_id < clients_[i]->GetSessionId() ||
                        name != clients_[i]->GetName()) {
                    return nullptr;
                }
                if (session_id > clients_[i]->GetSessionId()) {
                    clients_.erase(clients_.begin() + i);
                    CreateClient(address, session_id, name);
                    return clients_.back();
                }
                return clients_[i];
            }
        }
        return CreateClient(address, session_id, name);
    }

    int GetPlayerNamesSize() {
        uint32_t size = 0;
        for (uint32_t i = 0; i < clients_.size(); i++) {
            size += clients_[i]->GetName().size() + 1;
        }
        return size;
    }

    void HandleMessage(CtosMsg* message, sockaddr_in6& address) {
        Client *client =
                FindClientForSockaddrIn(address, message->session_id_,
                message->player_name_);

        if (client != NULL) {
            client->AddDataFromMsg(message);
        }
    }

    void AllLeaveGame() {
        for (uint32_t i = 0; i < clients_.size(); i++) {
            clients_[i]->LeaveGame();
        }
    }

    vector<Client*> GetClientsReadyAsPlayers() {
        DisconnectInactive();
        vector<Client*> ready;
        for (uint32_t i = 0; i < clients_.size(); i++) {
            if (clients_[i]->GetName() != "") {
                if (clients_[i]->IsReady()) {
                    ready.push_back(clients_[i]);
                } else {
                    ready.clear();
                    break;
                }
            }
        }
        return ready;
    }

private:
    Client* CreateClient(sockaddr_in6 address, uint64_t session_id, string name) {
        if (clients_.size() < 42 &&
            name.size() + GetPlayerNamesSize() <=
            MAX_PLAYER_NAMES_LIST_SIZE) {
            Client *client = new Client(address, session_id, name);
            clients_.push_back(client);
            return client;
        }
        return NULL;
    }

    void DisconnectInactive() {
        for (int i = 0; i < clients_.size(); i++) {
            if (!clients_[i]->IsActive()) {
                clients_.erase(clients_.begin() + i);
                i--;
            }
        }
    }
};

class GameController {
    uint32_t current_game_id_;
    bool game_is_active_;
    vector<Player> players_;
    Clients *clients_;
    vector<Event> events_;
    Board *board_;

public:
    GameController(Clients* clients, Board* board) :
            clients_(clients), board_(board), game_is_active_(false) {}

    uint32_t GetGameId() {
        return current_game_id_;
    }

    int GetNumAlivePlayers() {
        int alive_players = 0;
        for (uint32_t i = 0; i < players_.size(); i++) {
            if (players_[i].IsAlive()) {
                alive_players++;
            }
        }
        return alive_players;
    }

    void Tick() {
        if (game_is_active_) {
            if (GetNumAlivePlayers() <= 1) {
                events_.push_back(EventGameOver());
                clients_->AllLeaveGame();
                game_is_active_ = false;
                players_.clear();
            } else {
                MoveAlivePlayersForward();
            }
        } else {
            vector<Client*> clients_ready_as_players =
                    clients_->GetClientsReadyAsPlayers();
            if (clients_ready_as_players.size() >= 2) {
                game_is_active_ = true;
                current_game_id_ = Random();
                StartNewGame(clients_ready_as_players);
            }
        }
    }

    void MoveAlivePlayersForward() {
        for (uint32_t i = 0; i < players_.size(); i++) {
            Client* client = clients_->GetClientByName(players_[i].GetName());
            if (players_[i].IsAlive()) {
                int8_t turn_direction = 0;
                if (client != NULL && !client->IsObserver()) {
                    turn_direction = client->GetTurnDirection();
                }
                players_[i].MoveForward(turn_direction, board_);
                GeneratePlayerEvent(players_[i], i);
            }
        }
    }

    void StartNewGame(vector<Client*> clients_ready_as_players) {
        events_.clear();
        board_->ClearBoard();
        vector<string>player_names;
        for (Client* client_ready_as_player : clients_ready_as_players) {
            player_names.push_back(client_ready_as_player->GetName());
            client_ready_as_player->JoinGame();
        }
        sort(player_names.begin(), player_names.end());
        for (string player_name : player_names) {
            double x = (Random() % board_->GetWidth()) + 0.5;
            double y = (Random() % board_->GetHeight()) + 0.5;
            int direction = Random() % 360;
            players_.push_back(Player(x, y, direction, player_name, board_));
        }
        events_.push_back(EventNewGame(
                board_->GetWidth(), board_->GetHeight(), player_names));
        for (uint32_t i = 0; i < players_.size(); i++) {
            GeneratePlayerEvent(players_[i], i);
        }
    }

    void GeneratePlayerEvent(Player& player, uint8_t player_number) {
        if (!player.IsAlive()) {
            events_.push_back(EventPlayerEliminated(player_number));
        } else {
            events_.push_back(
                    EventPixel(player_number, player.GetX(), player.GetY()));
        }
    }

    const vector<Event>& GetEvents() {
        return events_;
    }

    uint32_t GetLastEventNo() {
        return events_.size() - 1;
    }

private:
    Event EventNewGame(uint32_t maxx, uint32_t maxy, vector<string>& player_names) {
        NewGame *new_game = new NewGame(maxx, maxy, player_names);
        uint32_t len = 5 + sizeof(maxx) + sizeof(maxy) +
                       clients_->GetPlayerNamesSize();
        return Event(len, events_.size(), 0, new_game);
    }

    Event EventPixel(uint8_t player_number, uint32_t x, uint32_t y) {
        Pixel *pixel = new Pixel(player_number, x, y);
        uint32_t len = 14;
        return Event(len, events_.size(), 1, pixel);
    }

    Event EventPlayerEliminated(uint8_t player_number) {
        PlayerEliminated *player_eliminated = new PlayerEliminated(player_number);
        uint32_t len = 6;
        return Event(len, events_.size(), 2, player_eliminated);
    }

    Event EventGameOver() {
        uint32_t len = 5;
        return Event(len, events_.size(), 3);
    }
};

void ParseArgs(int argc, char* argv[], int& width, int& height) {
    // defaults
    width = 800;
    height = 600;
    port_num = 12345;
    int game_speed = 50;
    turn_speed = 6;
    int option = 0;

    char* end;
    while((option = getopt(argc, argv, "W:H:p:s:t:r:")) != -1) {
        switch (option) {
            case 'W':
                end = optarg + strlen(optarg);
                width = strtoul(optarg, &end, 10);
                if (*end != 0 || width == 0) {
                    fprintf(stderr, "ERROR: Invalid width argument\n");
                    exit(1);
                }
                break;
            case 'H':
                end = optarg + strlen(optarg);
                height = strtoul(optarg, &end, 10);
                if (*end != 0 || height == 0) {
                    fprintf(stderr, "ERROR: Invalid height argument\n");
                    exit(1);
                }
                break;
            case 'p':
                end = optarg + strlen(optarg);
                port_num = strtoul(optarg, &end, 10);
                if (*end != 0 || port_num > MAX_PORT_NUM || port_num == 0) {
                    fprintf(stderr, "ERROR: Invalid port_num argument\n");
                    exit(1);
                }
                break;
            case 's':
                end = optarg + strlen(optarg);
                game_speed = strtoul(optarg, &end, 10);
                if (*end != 0 || game_speed == 0) {
                    fprintf(stderr, "ERROR: Invalid game_speed argument\n");
                    exit(1);
                }
                break;
            case 't':
                end = optarg + strlen(optarg);
                turn_speed = strtoul(optarg, &end, 10);
                if (*end != 0 || turn_speed == 0) {
                    fprintf(stderr, "ERROR: Invalid turn_speed argument\n");
                    exit(1);
                }
                break;
            case 'r':
                end = optarg + strlen(optarg);
                seed = strtoull(optarg, &end, 10);
                if (*end != 0) {
                    fprintf(stderr, "ERROR: Invalid seed argument\n");
                    exit(1);
                }
                break;
            default:
                fprintf(stderr, "Usage %s "
                        "[-W n] [-H n] [-p n] [-s n] [-t n] [-r n]", argv[0]);
                exit(1);
        }
    }

    game_turn_time_ms = 1000 / game_speed;
}

bool SendToClient(
        int sock, sockaddr_in6 client_address, char* message, int len) {
    socklen_t snda_len = (socklen_t) sizeof(client_address);
    int sflags = MSG_DONTWAIT;
    int snd_len = sendto(sock, message, len, sflags,
                         (const sockaddr*) &client_address, snda_len);
    if (snd_len < 0) {
        if (errno == EWOULDBLOCK || errno == EAGAIN) {
            return false;
        }
        else {
            fprintf(stderr, "ERROR: sending\n");
            exit(1);
        }
    }
    return true;
}

void SendEventsToClient(int sock, Client *client, const vector<Event>& events,
                        uint32_t game_id) {
    if (client == NULL || client->GetNextExpectedEventNo() >= events.size()) {
        return;
    }
    vector<Event> events_to_send;
    int size = 4;
    for (int i = client->GetNextExpectedEventNo(); i < events.size(); i++) {
        int event_size = events[i].len_ + 8;
        if (size + event_size > 512) {
            break;
        }
        events_to_send.push_back(events[i]);
        size += event_size;
    }
    StocMsg message(game_id, events_to_send);
    char buf[MAX_BUF_LENGTH];
    uint32_t len = message.Serialize(buf, MAX_BUF_LENGTH);
    if (len > 0) {
        if (SendToClient(sock, client->GetAddress(), buf, len)) {
            client->EventsSent(message.events_.size());
        }
    }
}

int ReceiveFromClient(
        int sock, sockaddr_in6 &client_address, char* buf) {
    socklen_t rcva_len = (socklen_t) sizeof(client_address);
    int flags = MSG_DONTWAIT;
    int len = recvfrom(sock, buf, MAX_BUF_LENGTH, flags,
                       (struct sockaddr *) &client_address, &rcva_len);
    if (len < 0) {
        if (errno == EWOULDBLOCK || errno == EAGAIN) {
            return -1;
        }
        else {
            fprintf(stderr, "ERROR: receiving\n");
        }
    }
    return len;
}

pair<CtosMsg*, sockaddr_in6> ReceiveMessage(int sock) {
    struct sockaddr_in6 client_address;
    char buf[MAX_BUF_LENGTH + 1];
    int len = ReceiveFromClient(sock, client_address, buf);
    if (len != -1) {
        try {
            CtosMsg *message = new CtosMsg(buf, len);
            return make_pair(message, client_address);
        } catch (InCorrectMessageException) {
            return make_pair(nullptr, client_address);
        }
    }
    return make_pair(nullptr, client_address);
}

int GetSock() {
    sockaddr_in6 my_address;
    int sock = socket(PF_INET6, SOCK_DGRAM, 0);
    if (sock < 0) {
        fprintf(stderr, "ERROR: socket\n");
        exit(1);
    }

    my_address.sin6_family = AF_INET6;
    my_address.sin6_addr = in6addr_any;
    my_address.sin6_port = htons(port_num);

    if (bind(sock, (struct sockaddr *) &my_address,
             (socklen_t) sizeof(my_address)) < 0) {
        fprintf(stderr, "ERROR: bind\n");
        exit(1);
    }
    return sock;
}

int main(int argc, char* argv[]) {
    int width, height;
    seed = time(NULL);
    ParseArgs(argc, argv, width, height);
    SetRandom(seed);
    int sock = GetSock();
    Board board(width, height);
    Timer timer(0);
    Clients clients;
    GameController game_controller(&clients, &board);

    for (;;) {
        if (timer.TimesUp()) {
            game_controller.Tick();
            timer.SetTimer(game_turn_time_ms);
        }

        fd_set read_fds;
        FD_ZERO(&read_fds);
        FD_SET(sock, &read_fds);

        fd_set write_fds;
        FD_ZERO(&write_fds);
        timeval *timeout = NULL;
        if (clients.IsAnyClientWaiting(game_controller.GetLastEventNo())) {
            FD_SET(sock, &write_fds);
        }
        timeout = new timeval;
        timer.GetTimeLeftAsTimeval(timeout);

        int result = select(sock + 1, &read_fds, &write_fds, NULL, timeout);
        if (timeout != NULL) {
            delete timeout;
        }
        if (result > 0) {
            if (FD_ISSET(sock, &write_fds)) {
                SendEventsToClient(
                        sock,
                        clients.GetNextClientToSend(
                                game_controller.GetLastEventNo()),
                        game_controller.GetEvents(),
                        game_controller.GetGameId());
            }
            if (FD_ISSET(sock, &read_fds)) {
                pair < CtosMsg*, sockaddr_in6> msg_address = ReceiveMessage(sock);
                if (msg_address.first != NULL) {
                    clients.HandleMessage(msg_address.first, msg_address.second);
                    delete msg_address.first;
                }
            }
        }
    }
}