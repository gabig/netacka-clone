#include "lib.h"

uint64_t act_rand;

bool operator==(sockaddr_in6 l, sockaddr_in6 p) {
    return (memcmp(&l.sin6_addr, &p.sin6_addr, 16 == 0) &&
            l.sin6_port == p.sin6_port);
}

bool operator<(sockaddr_in6 l, sockaddr_in6 p) {
    int addr_cmp = memcmp(&l.sin6_addr, &p.sin6_addr, 16);
    if (addr_cmp != 0) {
        return addr_cmp < 0;
    }
    return l.sin6_port < p.sin6_port;
}


Timer::Timer(uint64_t duration_ms) {
    SetTimer(duration_ms);
}

void Timer::SetTimer(uint64_t duration_ms) {
    end_time_ = GetTime() + 1000 * duration_ms;
}

uint64_t Timer::TimeLeft() {
    uint64_t act_time = GetTime();
    return end_time_ - act_time;
}

bool Timer::TimesUp() {
    uint64_t act_time = GetTime();
    return (act_time >= end_time_);
}

bool Timer::GetTimeLeftAsTimeval(struct timeval *time_left) {
    uint64_t time_left_ms = this->TimeLeft();
    if (TimesUp()) {
        return false;
    }
    time_left->tv_sec = time_left_ms / 1000000;
    time_left->tv_usec = time_left_ms % 1000000;
    return true;
}

int SerializeInt8(char* buf, int8_t num) {
    memcpy(buf, &num, sizeof(num));
    return sizeof(num);
}

int DeserializeInt8(char* buf, int8_t* num) {
    memcpy(num, buf, sizeof(*num));
    return sizeof(*num);
}

int SerializeUint8(char* buf, uint8_t num) {
    memcpy(buf, &num, sizeof(num));
    return sizeof(num);
}

int DeserializeUint8(char* buf, uint8_t* num) {
    memcpy(num, buf, sizeof(*num));
    return sizeof(*num);
}

int SerializeUint32(char* buf, uint32_t num) {
    uint32_t num_ns = htonl(num);
    memcpy(buf, &num_ns, sizeof(num));
    return sizeof(num);
}

int DeserializeUint32(char* buf, uint32_t* num) {
    uint32_t num_ns;
    memcpy(&num_ns, buf, sizeof(num_ns));
    *num = ntohl(num_ns);
    return sizeof(*num);
}

int SerializeUint64(char* buf, uint64_t num) {
    uint64_t num_ns = htobe64(num);
    memcpy(buf, &num_ns, sizeof(num_ns));
    return sizeof(num);
}

int DeserializeUint64(char* buf, uint64_t* num) {
    uint64_t num_ns;
    memcpy(&num_ns, buf, sizeof(num_ns));
    *num = be64toh(num_ns);
    return sizeof(*num);
}

int SerializeCharArray(char* buf, char* charArray) {
    uint32_t len = strlen(charArray);
    memcpy(buf, charArray, len);
    return len;
}

int DeserializeCharArray(char* buf, char** charArray, uint32_t len) {
    *charArray = new char[len];
    memcpy(*charArray, buf, len);
    return len;
}

int SerializeStringVector(char *buf, const vector <string> &v) {
    uint32_t size = 0;
    for (uint32_t i = 0; i < v.size(); i++) {
        memcpy(buf + size, v[i].c_str(), v[i].size());
        size += v[i].size();
        buf[size] = 0;
        size++;
    }
    return size;
}

int DeserializeStringVector(char *buf, vector <string> *v, uint32_t len) {
    uint32_t size = 0;
    string next = "";
    for (uint32_t i = 0; i < len; i++) {
        if (buf[i] == 0) {
            v->push_back(next);
            size += next.size();
            next = "";
        } else {
            next += buf[i];
        }
    }
    return size;
}

NewGame::NewGame(char* buf, uint32_t len) {
    uint32_t size = 0;
    size += DeserializeUint32(buf, &maxx_);
    size += DeserializeUint32(buf + size, &maxy_);
    if (len - size > 0) {
        size += DeserializeStringVector(buf + size, &player_names_, len - size);
    }
}

NewGame::NewGame(uint32_t maxx, uint32_t maxy, const vector<string>& player_names_) :
        maxx_(maxx), maxy_(maxy), player_names_(player_names_) {}

int NewGame::Serialize(char* buf, uint32_t len) {
    uint32_t size = 0;
    size += SerializeUint32(buf, maxx_);
    size += SerializeUint32(buf + size, maxy_);
    size += SerializeStringVector(buf + size, player_names_);
    return size;
}

Pixel::Pixel(uint8_t player_number, uint32_t x, uint32_t y) :
        player_number_(player_number), x_(x), y_(y) {}

Pixel::Pixel(char* buf, uint32_t len) {
    uint32_t size = 0;
    size += DeserializeUint8(buf, &player_number_);
    size += DeserializeUint32(buf + size, &x_);
    size += DeserializeUint32(buf + size, &y_);
}

int Pixel::Serialize(char* buf, uint32_t len) {
    uint32_t size = 0;
    size += SerializeUint8(buf, player_number_);
    size += SerializeUint32(buf + size, x_);
    size += SerializeUint32(buf + size, y_);
    return size;
}

PlayerEliminated::PlayerEliminated(uint8_t player_number) :
        player_number_(player_number) {}

PlayerEliminated::PlayerEliminated(char* buf, uint32_t len) {
    DeserializeUint8(buf, &player_number_);
}

int PlayerEliminated::Serialize(char* buf, uint32_t len) {
    return SerializeUint8(buf, player_number_);
}

Event::Event() {}

Event::Event(
        uint32_t len, uint32_t event_no, uint8_t event_type) :
        len_(len), event_no_(event_no), event_type_(event_type),
        new_game_event_data_(NULL), pixel_event_data_(NULL),
        player_eliminated_event_data_(NULL) {}

Event::Event(uint32_t len, uint32_t event_no, uint8_t event_type,
             NewGame *new_game) :
        len_(len), event_no_(event_no), event_type_(event_type),
        new_game_event_data_(new_game), pixel_event_data_(NULL),
        player_eliminated_event_data_(NULL) {}

Event::Event(uint32_t len, uint32_t event_no, uint8_t event_type,
             Pixel *pixel) :
        len_(len), event_no_(event_no), event_type_(event_type),
        new_game_event_data_(NULL), pixel_event_data_(pixel),
        player_eliminated_event_data_(NULL) {}

Event::Event(uint32_t len, uint32_t event_no, uint8_t event_type,
             PlayerEliminated *player_eliminated) :
        len_(len), event_no_(event_no), event_type_(event_type),
        new_game_event_data_(NULL), pixel_event_data_(NULL),
        player_eliminated_event_data_(player_eliminated) {}

Event::Event(char* buf, uint32_t len, int& read_bytes) {
    uint32_t size = 0;
    size += DeserializeUint32(buf, &len_);
    size += DeserializeUint32(buf + size, &event_no_);
    size += DeserializeUint8(buf + size, &event_type_);
    string name;
    int event_data_len = len_ - sizeof(event_no_) - sizeof(event_type_);
    uint32_t size_including_event_data_ = size + event_data_len;

    if (event_type_ > 3) {
        new_game_event_data_ = NULL;
        pixel_event_data_ = NULL;
        player_eliminated_event_data_ = NULL;
        return;
    }

    switch(event_type_) {
        case 0:
            new_game_event_data_.reset(
                    new NewGame(buf + size, event_data_len));
            pixel_event_data_ = NULL;
            player_eliminated_event_data_ = NULL;
            break;
        case 1:
            new_game_event_data_ = NULL;
            pixel_event_data_.reset(
                    new Pixel(buf + size, event_data_len));
            player_eliminated_event_data_ = NULL;
            break;
        case 2:
            new_game_event_data_ = NULL;
            pixel_event_data_ = NULL;
            player_eliminated_event_data_.reset(new PlayerEliminated(
                    buf + size, event_data_len));
            break;
        case 3:
            new_game_event_data_ = NULL;
            pixel_event_data_ = NULL;
            player_eliminated_event_data_ = NULL;
            break;
    }
    size += event_data_len;
    fflush(stdout);
    DeserializeUint32(buf + size, &crc32_);
    size += sizeof(crc32_);
    read_bytes = size;
}

int Event::Serialize(char* buf, uint32_t len) {
    uint32_t size = 0;
    size += SerializeUint32(buf, len_);
    size += SerializeUint32(buf + size, event_no_);
    size += SerializeUint8(buf + size, event_type_);
    string name;
    switch(event_type_) {
        case 0:
            size += new_game_event_data_->Serialize(buf + size, len - size);
            break;
        case 1:
            size += pixel_event_data_->Serialize(buf + size, len - size);
            break;
        case 2:
            size += player_eliminated_event_data_->Serialize(
                    buf + size, len - size);
            break;
        case 3:
            break;
    }
    uint32_t crc = GetCrc32((unsigned char*) buf, size);
    size += SerializeUint32(buf + size, crc);
    return size;
}

bool Event::CorrectCrc32() {
    char* buf = new char[MAX_BUF_LENGTH];
    int len = this->Serialize(buf, MAX_BUF_LENGTH);
    uint32_t crc32 = GetCrc32((unsigned char*) buf, len - 4);
    delete[] buf;
    return crc32 == crc32_;
}

NewGameCtogMsg::NewGameCtogMsg(shared_ptr<NewGame> new_game) {
    maxx_ = new_game->maxx_;
    maxy_ = new_game->maxy_;
    player_names_ = new_game->player_names_;
}

NewGameCtogMsg::NewGameCtogMsg(
        uint32_t maxx, uint32_t maxy, const vector <string> &player_names) :
        maxx_(maxx), maxy_(maxy), player_names_(player_names) {}

int NewGameCtogMsg::Serialize(char *buf) {
    uint32_t size = 0;
    string event_name = "NEW_GAME";
    string maxx_str = to_string(maxx_);
    string maxy_str = to_string(maxy_);

    strncpy(buf, event_name.c_str(), event_name.size());
    size += event_name.size();
    buf[size] = 32;
    size++;

    strncpy(buf + size, maxx_str.c_str(), maxx_str.size());
    size += maxx_str.size();
    buf[size] = 32;
    size++;

    strncpy(buf + size, maxy_str.c_str(), maxy_str.size());
    size += maxy_str.size();
    buf[size] = 32;
    size++;

    for (uint32_t i = 0; i < player_names_.size(); i++) {
        strncpy(buf + size, player_names_[i].c_str(), player_names_[i].size());
        size += player_names_[i].size();
        buf[size] = 32;
        size++;
    }
    buf[size - 1] = 10;
    return size;
}

PixelCtogMsg::PixelCtogMsg(shared_ptr<Pixel> pixel) {
    x_ = pixel->x_;
    y_ = pixel->y_;
    player_number_ = pixel->player_number_;
}

PixelCtogMsg::PixelCtogMsg(int x, int y, uint8_t player_number) :
        x_(x), y_(y), player_number_(player_number) {}

int PixelCtogMsg::Serialize(char *buf, vector<string>& player_names) {
    uint32_t size = 0;
    string event_name = "PIXEL";
    string x_str = to_string(x_);
    string y_str = to_string(y_);
    string player_name = player_names[player_number_];

    strncpy(buf, event_name.c_str(), event_name.size());
    size += event_name.size();
    buf[size] = 32;
    size++;

    strncpy(buf + size, x_str.c_str(), x_str.size());
    size += x_str.size();
    buf[size] = 32;
    size++;

    strncpy(buf + size, y_str.c_str(), y_str.size());
    size += y_str.size();
    buf[size] = 32;
    size++;

    strncpy(buf + size, player_name.c_str(), player_name.size());
    size += player_name.size();
    buf[size] = 10;
    size++;
    return size;
}

PlayerEliminatedCtogMsg::PlayerEliminatedCtogMsg(
        shared_ptr<PlayerEliminated> player_eliminated) {
    player_number_ = player_eliminated->player_number_;
}

PlayerEliminatedCtogMsg::PlayerEliminatedCtogMsg(uint8_t player_number) :
        player_number_(player_number) {}

int PlayerEliminatedCtogMsg::Serialize(char *buf, vector<string>& player_names) {
    uint32_t size = 0;
    string event_name = "PLAYER_ELIMINATED";
    string player_name = player_names[player_number_];

    strncpy(buf + size, event_name.c_str(), event_name.size());
    size += event_name.size();
    buf[size] = 32;
    size++;

    strncpy(buf + size, player_name.c_str(), player_name.size());
    size += player_name.size();
    buf[size] = 10;
    size++;

    return size;
}

CtosMsg::CtosMsg(uint64_t session_id, int8_t turn_direction,
                 uint32_t next_expected_event_no, string player_name) :
        session_id_(session_id), turn_direction_(turn_direction),
        next_expected_event_no_(next_expected_event_no),
        player_name_(player_name) {}

CtosMsg::CtosMsg(char *buf, uint32_t len) {
    uint32_t size = 0;
    size += DeserializeUint64(buf, &session_id_);
    size += DeserializeInt8(buf + size, &turn_direction_);
    size += DeserializeUint32(buf + size, &next_expected_event_no_);
    for (uint32_t i = size; i < len; i++) {
        player_name_ += buf[i];
    }
    size += player_name_.size();
    if (!IsPlayerNameValid(player_name_)) {
        throw InCorrectMessageException();
    }
    if (size != len) {
        throw InCorrectMessageException();
    }
}

int CtosMsg::Serialize(char *buf, uint32_t len) {
    uint32_t size = 0;
    size += SerializeUint64(buf, session_id_);
    size += SerializeInt8(buf + size, turn_direction_);
    size += SerializeUint32(buf + size, next_expected_event_no_);
    strncpy(buf + size, player_name_.c_str(), player_name_.size());
    size += player_name_.size();
    return size;
}

StocMsg::StocMsg(uint32_t game_id, vector <Event> &events) :
        game_id_(game_id), events_(events) {}

StocMsg::StocMsg(char *buf, uint32_t len) {
    uint32_t size = 0;
    size += DeserializeUint32(buf, &game_id_);
    int read_bytes;

    while(size < len) {
        Event event = Event(buf + size, len - size, read_bytes);
        size += read_bytes;
        if (event.CorrectCrc32()) {
            if (event.event_type_ <= 3) {
                events_.push_back(event);
            }
        } else {
            return;
        }
    }
}

int StocMsg::Serialize(char *buf, uint32_t len) {
    uint32_t size = 0;
    size += SerializeUint32(buf, game_id_);
    for (uint32_t i = 0; i < events_.size(); i++) {
        size += events_[i].Serialize(buf + size, len - size);
    }
    return size;
}

void SetRandom(uint64_t seed) {
    act_rand = seed;
}

uint64_t Random() {
    uint64_t prev_rand = act_rand;
    act_rand = (act_rand * 279470273) % 4294967291;
    return prev_rand;
}

pair<int, int> ConvertCoords(double x, double y) {
    int int_x = floor(x);
    int int_y = floor(y);
    return make_pair(int_x, int_y);
}

bool IsPlayerNameValid(string name) {
    if (name.size() < 1 || name.size() > 64) {
        return false;
    }
    for (uint32_t i = 0; i < name.size(); i++) {
        if (name[i] > 126 || name[i] < 33) {
            return false;
        }
    }
    return true;
}

uint64_t GetTime() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (uint64_t)tv.tv_sec * 1000000ULL + (uint64_t)tv.tv_usec;
}

uint32_t GetCrc32(unsigned char *message, int len) {
    int i, j;
    unsigned int byte, crc, mask;

    i = 0;
    crc = 0xFFFFFFFF;
    while (i < len) {
        byte = message[i];
        crc = crc ^ byte;
        for (j = 7; j >= 0; j--) {
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i = i + 1;
    }
    return ~crc;
}