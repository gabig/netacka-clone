#ifndef __LIB_H
#define __LIB_H

#define _BSD_SOURCE
#include <inttypes.h>
#include <netinet/tcp.h>
#include <endian.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <cmath>
#include <stdint.h>
#include <utility>
#include <string>
#include <vector>
#include <cstring>
#include <memory>
#include <getopt.h>
#include <queue>
#include <map>
#include <exception>

#define MAX_PORT_NUM 65535
#define MAX_BUF_LENGTH 1000
#define MAX_PLAYER_NAMES_LIST_SIZE 487

using namespace std;

class InCorrectMessageException : public exception {};

bool operator==(sockaddr_in6 l, sockaddr_in6 p);

bool operator<(sockaddr_in6 l, sockaddr_in6 p);

class Timer {
    uint64_t end_time_;
public:
    Timer(uint64_t duration_ms);
    void SetTimer(uint64_t duration_ms);
    uint64_t TimeLeft();
    bool TimesUp();
    bool GetTimeLeftAsTimeval(struct timeval *time_left);
};

int SerializeUint8(char* buf, uint8_t num);

int DeserializeUint8(char* buf, uint8_t* num);

int SerializeUint32(char* buf, uint32_t num);

int DeserializeUint32(char* buf, uint32_t* num);

int SerializeCharArray(char* buf, char* charArray);

int DeserializeCharArray(char* buf, char* charArray);

int SerializeStringVector(char *buf, const vector <string> &v);

int DeserializeStringVector(char *buf, vector <string> *v, uint32_t len);

// structs for events
struct NewGame {
    uint32_t maxx_;
    uint32_t maxy_;
    vector<string>player_names_;

    NewGame(uint32_t maxx, uint32_t maxy, const vector<string>&player_names_);
    NewGame(char* buf, uint32_t len);
    int Serialize(char* buf, uint32_t len);
    vector<string> GetPlayerNames();
};

struct Pixel {
    uint8_t player_number_;
    uint32_t x_;
    uint32_t y_;

    Pixel(uint8_t player_number, uint32_t x, uint32_t y);
    Pixel(char* buf, uint32_t len);
    int Serialize(char* buf, uint32_t len);
};

struct PlayerEliminated {
    uint8_t player_number_;

    PlayerEliminated(uint8_t player_number);
    PlayerEliminated(char* buf, uint32_t len);
    int Serialize(char* buf, uint32_t len);
};

struct Event {
    uint32_t len_;
    uint32_t event_no_;
    uint8_t event_type_;
    shared_ptr<NewGame> new_game_event_data_;
    shared_ptr<Pixel> pixel_event_data_;
    shared_ptr<PlayerEliminated> player_eliminated_event_data_;
    uint32_t crc32_;

    Event();
    Event(uint32_t len, uint32_t event_no, uint8_t event_type, NewGame *new_game);
    Event(uint32_t len, uint32_t event_no, uint8_t event_type, Pixel *pixel);
    Event(uint32_t len, uint32_t event_no, uint8_t event_type,
          PlayerEliminated *player_eliminated);
    Event(uint32_t len, uint32_t event_no, uint8_t event_type);
    Event(char* buf, uint32_t len, int& read_bytes);
    int Serialize(char* buf, uint32_t len);
    bool CorrectCrc32();
};

//client to GUI messages
struct NewGameCtogMsg {
    uint32_t maxx_;
    uint32_t maxy_;
    vector<string>player_names_;

    NewGameCtogMsg(shared_ptr<NewGame> new_game);
    NewGameCtogMsg(uint32_t maxx, uint32_t maxy,
                   const vector<string>& player_names);
    int Serialize(char* buf);
};

struct PixelCtogMsg {
    int x_;
    int y_;
    uint8_t player_number_;

    PixelCtogMsg(shared_ptr<Pixel> pixel);
    PixelCtogMsg(int x, int y, uint8_t player_number);
    int Serialize(char* buf, vector<string>& player_names);
};

struct PlayerEliminatedCtogMsg {
    uint8_t player_number_;

    PlayerEliminatedCtogMsg(shared_ptr<PlayerEliminated> player_eliminated);
    PlayerEliminatedCtogMsg(uint8_t player_number);
    int Serialize(char* buf, vector<string>& player_names);
};

// client to server message
struct CtosMsg {
    uint64_t session_id_;
    int8_t turn_direction_;
    uint32_t next_expected_event_no_;
    string player_name_;

    CtosMsg(uint64_t session_id, int8_t turn_direction,
            uint32_t next_expected_event_no, string player_name);
    CtosMsg(char* buf, uint32_t len);
    int Serialize(char* buf, uint32_t len);
};

// server to client message
struct StocMsg {
    uint32_t game_id_;
    vector<Event>events_;

    StocMsg(uint32_t game_id, vector<Event>& events);
    StocMsg(char* buf, uint32_t len);
    int Serialize(char* buf, uint32_t len);
};

void SetRandom(uint64_t seed);

uint64_t Random();

pair<int, int> ConvertCoords(double x, double y);

bool IsPlayerNameValid(string name);

uint64_t GetTime();

uint32_t GetCrc32(unsigned char* buf, int len);

#endif