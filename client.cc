#include "lib.h"

string player_name;
int8_t turn_direction;
uint32_t next_expected_event_no, game_id;
uint64_t session_id;
uint32_t next_event_to_GUI_no;
map<uint32_t, Event> events;
uint8_t gui_IPv, server_IPv;
int gui_sock;
bool waiting_for_new_game;
vector<string> player_names;
struct sockaddr_in server_address;
struct sockaddr_in6 server_address6;
socklen_t snda_len;
sockaddr* server_addr;

int CountColons(char *address_arg) {
    int count = 0;
    for (uint32_t i = 0; i < strlen(address_arg); i++) {
        if (address_arg[i] == ':') {
            count++;
        }
    }
    return count;
}

void PrepareToNewGame() {
    next_expected_event_no = 0;
    next_event_to_GUI_no = 0;
    events.clear();
    waiting_for_new_game = false;
}

void SetServerAddress(string name_or_address, int port_num) {
    struct addrinfo addr_hints;
    struct addrinfo *addr_result;

    (void) memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family = PF_UNSPEC;
    addr_hints.ai_socktype = SOCK_DGRAM;
    addr_hints.ai_protocol = IPPROTO_UDP;

    int error = getaddrinfo(name_or_address.c_str(), NULL, &addr_hints,
                            &addr_result);
    if (error != 0) {
        fprintf(stderr, "ERROR: Invalid host or address\n");
        exit(1);
    }
    if (addr_result->ai_family == AF_INET) {
        server_address.sin_addr.s_addr =
                ((struct sockaddr_in *) (addr_result->ai_addr))->sin_addr.s_addr;
        server_address.sin_port = htons((uint16_t) port_num);
        server_addr = (sockaddr*) &server_address;
        snda_len = sizeof(sockaddr_in);
        server_IPv = 4;
    } else if (addr_result->ai_family == AF_INET6) {
        memcpy(&server_address6.sin6_addr,
               &((struct sockaddr_in6 *) (addr_result->ai_addr))->sin6_addr, 16);
        server_address6.sin6_port = htons((uint16_t) port_num);
        server_addr = (sockaddr*) &server_address6;
        snda_len = sizeof(sockaddr_in6);
        server_IPv = 6;
    } else {
        fprintf(stderr, "ERROR: Unknown server address format\n");
        exit(1);
    }
    server_address.sin_family = addr_result->ai_family;
    freeaddrinfo(addr_result);

}

void SetGuiSock(string name_or_address, string port_num) {
    struct addrinfo addr_hints;
    struct addrinfo *addr_result;

    (void) memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family = PF_UNSPEC;
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;

    int error = getaddrinfo(name_or_address.c_str(), port_num.c_str(),
                            &addr_hints, &addr_result);
    if (error != 0) {
        fprintf(stderr, "ERROR: gataddrinfo %s\n", gai_strerror(error));
        exit(1);
    }
    if (addr_result->ai_family == AF_INET) {
        gui_IPv = 4;
    } else if (addr_result->ai_family == AF_INET6) {
        gui_IPv = 6;
    } else {
        fprintf(stderr, "ERROR: Unknown server address format\n");
        exit(1);
    }

    gui_sock = socket(addr_result->ai_family, addr_result->ai_socktype,
                      addr_result->ai_protocol);
    if (gui_sock < 0) {
        fprintf(stderr, "ERROR: failed to open socket\n");
        exit(1);
    }

    int result = connect(
            gui_sock, addr_result->ai_addr, addr_result->ai_addrlen);
    if (result < 0) {
        fprintf(stderr, "ERROR: Connect with GUI\n");
        exit(1);
    }

    int no = 0;
    result = setsockopt(gui_sock, IPPROTO_IPV6, IPV6_V6ONLY,
                            (void *) &no, sizeof(no));
    if (result < 0) {
        fprintf(stderr, "ERROR: Setting NODELAY\n");
        exit(1);
    }

    freeaddrinfo(addr_result);
}

void ParseArgs(int argc, char* argv[]) {
    // defaults
    uint16_t server_port_num = 12345;
    string server_name = "localhost";
    string gui_port = "12346";

    if (argc < 3 || argc > 4) {
        fprintf(stderr,
                "Usage: %s player_name game_server_host[:port] "
                        "[ui_server_host[:port]]\n", argv[0]);
        exit(1);
    }

    player_name = argv[1];
    if (player_name != "" && !IsPlayerNameValid(player_name)) {
        fprintf(stderr, "ERROR: Invalid player name\n");
        exit(1);
    }

    int colons = CountColons(argv[2]);

    if (colons == 1) {
        char* colon = strchr(argv[2], ':');
        *colon = '\0';
        char* port = colon + 1;

        char* end = port + strlen(port);
        server_port_num = strtoul(port, &end, 10);
        if (*end != 0) {
            fprintf(stderr, "ERROR: Invalid port_num for game_server\n");
            exit(1);
        }
        SetServerAddress(argv[2], server_port_num);
    } else {
        SetServerAddress(argv[2], server_port_num);
    }

    if (argc == 4) {
        colons = CountColons(argv[3]);

        if (colons == 1) {
            char* colon = strchr(argv[3], ':');
            *colon = '\0';
            char* port = colon + 1;

            char* end = port + strlen(port);
            gui_port = port;
            if (*end != 0) {
                fprintf(stderr, "ERROR: Invalid port_num for game_server\n");
                exit(1);
            }
            SetGuiSock(argv[3], gui_port);
        } else {
            SetGuiSock(argv[3], gui_port);
        }
    } else {
        SetGuiSock("localhost", gui_port);
    }
}

int ServerSocket() {
    int sock = 0;
    int domain;

    if (server_IPv == 4) {
        domain = PF_INET;
    } else {
        domain = PF_INET6;
    }

    sock = socket(domain, SOCK_DGRAM, 0);
    if (sock < 0) {
        fprintf(stderr, "ERROR: socket\n");
        exit(1);
    }

    return sock;
}

bool IsMessageActual(struct StocMsg *message) {
    if (waiting_for_new_game && message->game_id_ != game_id) {
        game_id = message->game_id_;
        PrepareToNewGame();
    }
    return message->game_id_ == game_id;
}

void ProccessEvents(vector<Event> &new_events) {
    for (uint32_t i = 0; i < new_events.size(); i++) {
        events[new_events[i].event_no_] = new_events[i];
        if (new_events[i].event_no_ == next_expected_event_no) {
            next_expected_event_no++;
        }
        if (new_events[i].event_type_ == 0) {
            player_names = new_events[i].new_game_event_data_->player_names_;
        }
    }
}

void SendToServer(int sock, char *buf, int len) {
    int sflags = 0;
    int snd_len = sendto(sock, buf, len, sflags, server_addr, snda_len);
    if (snd_len < 0) {
        fprintf(stderr, "ERROR: sending\n");
        exit(1);
    }
}

void SendMessage(int sock) {
    CtosMsg *message = new CtosMsg(
            session_id, turn_direction, next_expected_event_no, player_name);
    int len = sizeof(session_id) + sizeof(turn_direction) +
              sizeof(next_expected_event_no) + player_name.size();
    char *buf = new char[len];
    message->Serialize(buf, len);

    SendToServer(sock, buf, len);

    delete message;
    delete[] buf;
}

void ReceiveFromServer(int sock) {
    char *buf = new char[MAX_BUF_LENGTH + 1];
    socklen_t rcva_len = (socklen_t) sizeof(server_address);
    int flags = MSG_DONTWAIT;
    int len = recvfrom(sock, buf, MAX_BUF_LENGTH, flags,
                       (struct sockaddr *) &server_address, &rcva_len);
    if (len < 0) {
        if (errno == EWOULDBLOCK || errno == EAGAIN) {
            delete[] buf;
            return;
        }
        else {
            fprintf(stderr, "ERROR: receiving message\n");
            exit(1);
        }
    }
    StocMsg *message = new StocMsg(buf, len);

    if (!IsMessageActual(message)) {
        delete message;
        delete[] buf;
        return;
    }

    ProccessEvents(message->events_);

    delete message;
    delete[] buf;
    return;
}

void ReceiveFromGUI(int sock) {
    char *buf = new char[15];
    memset(buf, 0, 15);
    int len = 0;

    do {
        int result = read(sock, buf + len, 1);
        if (result < 0) {
            fprintf(stderr, "ERROR: Reading from GUI failed\n");
            exit(1);
        }
        len += result;
    } while(buf[len - 1] != '\n');
    buf[len - 1] = 0;

    if (len > 0) {
        if (!strcmp(buf, "LEFT_KEY_DOWN")) {
            turn_direction = -1;
        } else if (!strcmp(buf, "LEFT_KEY_UP")) {
            turn_direction = 0;
        } else if (!strcmp(buf, "RIGHT_KEY_DOWN")) {
            turn_direction = 1;
        } else if (!strcmp(buf, "RIGHT_KEY_UP")) {
            turn_direction = 0;
        }
    }
    delete[] buf;
}

void SendToGUI(int sock) {
    uint8_t event_type = events[next_event_to_GUI_no].event_type_;

    char* buf = new char[MAX_BUF_LENGTH + 1];
    int len = 0;

    if (event_type == 0) {
        NewGameCtogMsg msg = NewGameCtogMsg(
                events[next_event_to_GUI_no].new_game_event_data_);
        len = msg.Serialize(buf);
    }
    if (event_type == 1) {
        PixelCtogMsg msg =
                PixelCtogMsg(events[next_event_to_GUI_no].pixel_event_data_);
        len = msg.Serialize(buf, player_names);
    }
    if (event_type == 2) {
        PlayerEliminatedCtogMsg msg = PlayerEliminatedCtogMsg(
                events[next_event_to_GUI_no].player_eliminated_event_data_);
        len = msg.Serialize(buf, player_names);
    }
    if (event_type == 3) {
        waiting_for_new_game = true;
        delete[] buf;
        return;
    }

    uint32_t size = 0;
    while (size < len) {
        size += write(sock, buf + size, len - size);
    }

    next_event_to_GUI_no++;

    delete[] buf;
}

int main(int argc, char* argv[]) {
    waiting_for_new_game = true;
    next_expected_event_no = 0;
    session_id = GetTime();

    ParseArgs(argc, argv);
    int server_sock = ServerSocket();

    Timer timer = Timer(0);
    for (;;) {
        fd_set read_fds;
        FD_ZERO(&read_fds);
        FD_SET(server_sock, &read_fds);
        FD_SET(gui_sock, &read_fds);

        fd_set write_fds;
        FD_ZERO(&write_fds);

        if (timer.TimesUp()) {
            FD_SET(server_sock, &write_fds);
        }

        if (events.find(next_event_to_GUI_no) != events.end()) {
            FD_SET(gui_sock, &write_fds);
        }

        timeval *timeout = new timeval;
        if (!timer.GetTimeLeftAsTimeval(timeout)) {
            delete timeout;
            timeout = NULL;
        }
        int result = select(max(gui_sock, server_sock) + 1, &read_fds,
                            &write_fds, NULL, timeout);
        if (timeout != NULL) {
            delete timeout;
        }
        if (result > 0) {
            if (FD_ISSET(server_sock, &write_fds)) {
                SendMessage(server_sock);
                timer.SetTimer(20);
            }

            if (FD_ISSET(gui_sock, &write_fds)) {
                SendToGUI(gui_sock);
            }

            if (FD_ISSET(gui_sock, &read_fds)) {
                ReceiveFromGUI(gui_sock);
            }

            if (FD_ISSET(server_sock, &read_fds)) {
                ReceiveFromServer(server_sock);
            }
        }
    }
}