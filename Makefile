all: siktacka-client siktacka-server

siktacka-client: client.cc lib.cc
	g++ -o siktacka-client -ggdb -Wall -O2 -std=c++14 client.cc lib.cc

siktacka-server: server.cc lib.cc
	g++ -o siktacka-server -ggdb -Wall -O2 -std=c++14 server.cc lib.cc

clean:
	rm -f siktacka-client siktacka-server